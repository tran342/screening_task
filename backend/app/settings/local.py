from .base import *  # noqa

DEBUG = False
PRODUCTION = False

# TODO: let's enable postgress here
# ANSWER: Postgres enabled
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ob',
        'USER': 'ob',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'OPTIONS': {'client_encoding': 'utf8'}
    }
}

STATIC_ROOT = '/var/static'
MEDIA_ROOT = '/var/media/pictures'
