#!/bin/sh
set -ex

until psql $DATABASE_URL -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing"
>&2 echo $DJANGO_SETTINGS_MODULE

if [ "$DJANGO_MANAGEPY_MIGRATE" = 'yes' ]; then
  python manage.py migrate --noinput
fi

if [ "$DJANGO_MANAGEPY_COLLECTSTATIC" = 'yes' ]; then
  python manage.py collectstatic --noinput
fi

if [ "$DJANGO_MANAGEPY_COMPILEMESSAGES" = 'yes' ]; then
  python manage.py compilemessages
fi

if [ "$DJANGO_MANAGEPY_POPULATE" = 'yes' ]; then
  python manage.py populate
fi

exec "$@"
