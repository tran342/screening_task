from rest_framework import serializers

from core.models import OfficeSpace


class OfficeSpaceSerializer(serializers.ModelSerializer):
    """ TODO: provide Meta """
    """ ANSWER: Added """
    class Meta:
        model = OfficeSpace
        fields = '__all__'
