from decimal import Decimal
import random

import factory


class OfficeSpaceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.OfficeSpace'

    name = factory.Faker('name')
    street = factory.Faker('name')
    city = factory.Faker('name')
    zip_code = '01-123'
    lat = Decimal(random.randint(52, 53))
    lng = Decimal(random.randint(20, 21))
