from rest_framework import viewsets

from core.models import OfficeSpace
from core.serializers import OfficeSpaceSerializer


class OfficeSpaceViewSet(viewsets.ModelViewSet):
    """TODO: at least queryset and serializer_class are missing """
    """ ANSWER: Added """
    queryset = OfficeSpace.objects.filter(public=True)
    serializer_class = OfficeSpaceSerializer
