from django.contrib import admin

from core.models import OfficeSpace


@admin.register(OfficeSpace)
class OfficeSpaceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'city')
