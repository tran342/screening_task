from decimal import Decimal
import random
import uuid

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand

from core.constants import LANDLORD, USER_CHOICES
from core.models import CustomUser, OfficeSpace

# from django.conf import settings

User = get_user_model()


def office_space_factory(number):
    building_names = [
        'Central Tower', 'Centrum LIM', 'Generation Park', 'Intraco I',
        'Millennium Plaza', 'Oxford Tower', 'Palace of Culture and Science',
    ]
    for i in range(number):
        yield {
            # TODO make building names unique
            # ANSWER: Add an UUID to it
            'name': '%s %s' % (str(uuid.uuid4()), building_names[i % len(building_names)]),
            'street': 'Street 1',
            'city': 'City',
            'zip_code': '01-123',
            'lat': Decimal(random.randint(52, 53)),
            'lng': Decimal(random.randint(20, 21)),
        }


def user_factory(number):
    # ANSWER User factory
    user_types = [item[0] for item in USER_CHOICES]
    for i in range(number):
        user_type = user_types[i % len(user_types)]
        yield {
            'email': '%s@%s.%s' % (str(uuid.uuid4()), user_type, user_type),
            'type': user_type
        }


class Command(BaseCommand):
    """
    Populates local database for development with sample random
    objects.

    Never to be used in production.
    # TODO: prevent running in production.

    """

    # TODO add user factory and user number argument
    def add_arguments(self, parser):
        parser.add_argument(
            '--office-spaces',
            type=int,
            default=10
        )
        parser.add_argument(
            '--users',
            type=int,
            default=10
        )

    def handle(self, *args, **options):
        # ANSWER: Prevent to execute in production
        # But because the test require to run populate in the readme so I comment it out
        # if settings.PRODUCTION:
        #    raise Exception('Sorry this command cannot be used in production')

        for office_space in office_space_factory(options['office_spaces']):
            OfficeSpace.objects.create(**office_space)

        # TODO create superuser
        # ANSWER Add super user
        CustomUser.objects.create(type=LANDLORD, email='admin@admin.admin', is_superuser=True)
        for user in user_factory(options['users']):
            CustomUser.objects.create(**user)
