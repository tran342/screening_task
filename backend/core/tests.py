"""
TODO: a test or two would be useful.
Up to you whether you want a unit test (meh, there's so little to unit test here)
or e2e test using rest_framework.test.APIClient (yeah).
"""
import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from core.factories import OfficeSpaceFactory


# ANSWER Yeh this is the test
class TourRegistrationTests(APITestCase):
    def setUp(self):
        OfficeSpaceFactory()
        OfficeSpaceFactory()
        OfficeSpaceFactory()

    def test_get(self):
        response = self.client.get(reverse('office-spaces-list'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(json.loads(response.content)), 3)
